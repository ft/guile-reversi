;; Copyright (c) 2021-2022 guile-reversi workers, All rights reserved.
;;
;; Terms for redistribution and use can be found in LICENCE.

(define-module (reversi game)
  #:use-module (ice-9 arrays)
  #:use-module (ice-9 match)
  #:use-module (ice-9 optargs)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-42)
  #:export (make-game
            game?
            game:size
            game:moves set-game-moves!
            game:positions set-game-positions!
            game:player set-game-player!
            game-count
            game-winner
            game-dup!
            game-move!
            make-location
            location?
            loc:file
            loc:rank
            loc
            on-board?
            valid-move?
            valid-moves
            valid-moves?
            opposite
            annotate-position
            discard-empty
            file-name
            file->offset
            rank->offset
            *default-starting-position*))

(define-record-type <location>
  (make-location file rank)
  location?
  (file loc:file)
  (rank loc:rank))

(define place-disc!
  (match-lambda*
    ((board tag f r) (array-set! board tag (1- r) (1- f)))
    ((board tag l) (place-disc! board tag (loc:file l) (loc:rank l)))))

(define (place-discs! board . lst)
  (for-each (lambda (p) (apply place-disc! board p)) lst))

(define peek-location
  (match-lambda*
    ((board f r) (array-ref board (1- r) (1- f)))
    ((board l) (peek-location board (loc:file l) (loc:rank l)))))

(define (tagged-location? board tag . args)
  (eq? tag (apply peek-location board args)))

(define (empty-location? board . args)
  (apply tagged-location? board '_ args))

(define* (make-board #:key (dimension 8) discs)
  (let ((board (make-array '_ dimension dimension)))
    (when discs
      (apply place-discs! board discs))
    board))

(define *default-starting-position* (make-board #:discs '((white 4 4)
                                                          (white 5 5)
                                                          (black 4 5)
                                                          (black 5 4))))

(define (opposite tag)
  (cond ((eq? tag 'white) 'black)
        ((eq? tag 'black) 'white)
        (else (error (format #f "Invalid colour: ~s" tag)))))

(define (get-board-size board)
  (apply min (array-dimensions board)))

(define (location-candidate? board dim diff)
  (let ((d2 (+ dim (* 2 diff)))
        (lim (get-board-size board)))
    (and (>= d2 1) (<= d2 lim))))

(define (valid-index? b n)
  (and (> n 0) (<= n (get-board-size b))))

(define (on-board? b f r)
  (and (valid-index? b f)
       (valid-index? b r)))

(define (candidate? board tag file rank df dr)
  (let ((opponent (opposite tag))
        (not-valid? (lambda (n) (not (valid-index? board n)))))
    (and (tagged-location? board opponent (+ file df) (+ rank dr))
         (let loop ((f (+ file (* 2 df)))
                    (r (+ rank (* 2 dr))))
           (cond ((not-valid? f) #f)
                 ((not-valid? r) #f)
                 ((empty-location? board f r) #f)
                 ((tagged-location? board tag f r) #t)
                 (else (loop (+ f df) (+ r dr))))))))

(define (candidate-direction? board tag file rank df dr)
  (and (location-candidate? board rank dr)
       (location-candidate? board file df)
       (candidate? board tag file rank df dr)))

(define *all-directions*
  (filter (lambda (d)
            (not (equal? d '(0 0 ))))
          (list-ec (: a -1 2) (: b -1 2) (list a b))))

(define (valid-move? board tag . args)
  (match args
    ((file rank) (and (empty-location? board file rank)
                      (any (lambda (d)
                             (apply candidate-direction? board tag file rank d))
                           *all-directions*)))
    ((l) (valid-move? board tag (loc:file l) (loc:rank l)))))

(define (valid-moves board tag)
  (let ((lim (get-board-size board)))
    (let next ((f lim) (r lim) (acc '()))
      (if (zero? r) acc
          (next (if (= f 1) lim (1- f))
                (if (= f 1) (1- r) r)
                (if (valid-move? board tag f r)
                    (cons (make-location f r) acc)
                    acc))))))

(define (valid-moves? board tag)
  (not (null? (valid-moves board tag))))

(define-record-type <game>
  (make-game* size moves positions player)
  game?
  (size game:size)
  (moves game:moves set-game-moves!)
  (positions game:positions set-game-positions!)
  (player game:player set-game-player!))

(define* (make-game #:key (size 8) (moves '())
                    (positions (list *default-starting-position*)))
  (make-game* size moves positions 'black))

(define (game-count game)
  (let* ((board (car (game:positions game)))
         (lim (get-board-size board)))
    (let next ((f lim) (r lim) (b 0) (w 0))
      (if (zero? r)
          (list b w (- (* lim lim) b w))
          (let ((this (peek-location board f r)))
            (next (if (= f 1) lim (1- f))
                  (if (= f 1) (1- r) r)
                  (if (eq? this 'black) (1+ b) b)
                  (if (eq? this 'white) (1+ w) w)))))))

(define (game-winner count)
  (match count
    ((b w e) (cond ((> b w) 'black)
                   ((< b w) 'white)
                   (else 'draw)))))

(define (game-dup! game)
  (let ((ps (game:positions game)))
    (set-game-positions! game (cons (array-copy (car ps)) ps))))

(define (game-add-move! game f r)
  (set-game-moves! game (cons (make-location f r) (game:moves game))))

(define (flip-discs-in-direction! board tag f r df dr)
  (let loop ((f (+ f df)) (r (+ r dr)))
    (unless (tagged-location? board tag f r)
      (place-disc! board tag f r)
      (loop (+ f df) (+ r dr)))))

(define (flip-discs! board tag f r)
  (for-each (lambda (d)
              (when (apply candidate-direction? board tag f r d)
                (apply flip-discs-in-direction! board tag f r d)))
            *all-directions*))

(define game-move!
  (match-lambda*
    ((game tag f r) (begin (game-dup! game)
                           (place-disc! (car (game:positions game)) tag f r)
                           (flip-discs! (car (game:positions game)) tag f r)
                           (game-add-move! game f r)))
    ((game tag l) (game-move! game tag (loc:file l) (loc:rank l)))))

(define (annotate-position p)
  (apply append
         (let loop ((r 1) (ranks (array->list p)))
           (if (null? ranks)
               ranks
               (cons (zip (list-ec (: n 1 9) n)
                          (list-ec (: n 8) r)
                          (car ranks))
                     (loop (1+ r) (cdr ranks)))))))

(define (discard-empty annotated-position)
  (filter (lambda (x)
            (match x
              ((f r '_) #f)
              (_        #t)))
          annotated-position))

(define *alphabet* '(a b c d e f g h i j k l m n o p q r s t u v w x y z))

(define (file-name n)
  (list-ref *alphabet* (1- n)))

(define (loc l)
  (list (file-name (loc:file l))
        (loc:rank l)))
