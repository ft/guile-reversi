;; Copyright (c) 2021-2022 guile-reversi workers, All rights reserved.
;;
;; Terms for redistribution and use can be found in LICENCE.

(define-module (reversi program)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-42)
  #:use-module (chickadee)
  #:use-module (reversi input keyboard)
  #:use-module (reversi input mouse)
  #:use-module (reversi ui)
  #:export (main))

(define *name* 'guile-reversi)
(define *version* '(0 1 0))

(define (version->string v)
  (string-join (map number->string v) "."))

(define (make-title)
  (format #f "~a v~a" *name* (version->string *version*)))

(define (main)
  (run-game #:window-height (geometry:height (ui:geometry reversi-ui))
            #:window-width  (geometry:width (ui:geometry reversi-ui))
            #:window-title  (make-title)
            #:load          init
            #:draw          draw
            #:key-release   keyboard-update
            #:mouse-release mouse-update
            #:update-hz     20))
