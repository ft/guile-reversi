;; Copyright (c) 2021-2022 guile-reversi workers, All rights reserved.
;;
;; Terms for redistribution and use can be found in LICENCE.

(define-module (reversi input mouse)
  #:use-module (srfi srfi-1)
  #:use-module (reversi game)
  #:use-module (reversi ui)
  #:export (mouse-update))

(define (mouse-update button x y)
  (let ((l (coordinate->location x y))
        (player (game:player reversi-game)))
    (format #t "DEBUG: ~s ~a~%" button (and (location? l) (loc l)))
    (when (and (eq? button 'left)
               (location? l)
               (valid-move? (car (game:positions reversi-game)) player l))
      (game-move! reversi-game player l)
      (format #t "Game move log (old to new): ~a~%"
              (reverse (map loc (game:moves reversi-game))))
      (if (valid-moves? (car (game:positions reversi-game)) (opposite player))
          (set-game-player! reversi-game (opposite player))
          (format #t "--- Out of moves: ~a - Sticking with ~s ---~%"
                  (opposite player) player))
      (let ((next-player (game:player reversi-game)))
        (if (valid-moves? (car (game:positions reversi-game))
                          next-player)
            (format #t "Valid Moves for ~a: ~a~%"
                    (game:player reversi-game)
                    (map loc (valid-moves (car (game:positions reversi-game))
                                          (game:player reversi-game))))
            (let* ((cnt (game-count reversi-game))
                   (winner (game-winner cnt)))
              (format #t "--- No valid moves left. ---~%")
              (format #t "--- Winner: ~a ~a ---~%" winner
                      (zip '(black white empty) cnt)))))
      (update-discs!))))
