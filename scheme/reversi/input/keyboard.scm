;; Copyright (c) 2021-2022 guile-reversi workers, All rights reserved.
;;
;; Terms for redistribution and use can be found in LICENCE.

(define-module (reversi input keyboard)
  #:export (keyboard-update))

(define (keyboard-update key modifiers)
  (format #t "DEBUG: ~a ~a~%" key modifiers))
