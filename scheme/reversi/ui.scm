;; Copyright (c) 2021-2022 guile-reversi workers, All rights reserved.
;;
;; Terms for redistribution and use can be found in LICENCE.

(define-module (reversi ui)
  #:use-module (ice-9 match)
  #:use-module (ice-9 optargs)
  #:use-module (srfi srfi-9)
  #:use-module (srfi srfi-42)
  #:use-module (chickadee graphics color)
  #:use-module (chickadee graphics path)
  #:use-module (chickadee math)
  #:use-module (chickadee math vector)
  #:use-module (reversi game)
  #:export (make-geometry
            geometry?
            geometry:width   set-geometry-width!
            geometry:height  set-geometry-height!
            geometry:margin  set-geometry-margin!
            make-theme
            theme?
            theme:ui-bg       set-theme-ui-bg!
            theme:board-bg    set-theme-board-bg!
            theme:board-grid  set-theme-board-grid!
            theme:disc-black  set-theme-disc-black!
            theme:disc-white  set-theme-disc-white!
            make-ui-cache
            ui-cache?
            ui-cache:origin      set-ui-cache-origin!
            ui-cache:size        set-ui-cache-size!
            ui-cache:spacing     set-ui-cache-spacing!
            ui-cache:line-width  set-ui-cache-line-width!
            ui-cache:radius      set-ui-cache-radius!
            make-ui
            ui?
            ui:cache        set-ui-cache!
            ui:geometry     set-ui-geometry!
            ui:theme        set-ui-theme!
            ui:game-canvas  set-ui-game-canvas!
            ui:disc-canvas  set-ui-disc-canvas!
            draw init update-discs!
            board-origin
            board-size
            grid-spacing
            grid-line-width
            disc-radius
            x->file
            y->rank
            coordinate->location
            reversi-game
            reversi-ui))

(define-record-type <geometry>
  (make-geometry* width height margin)
  geometry?
  (width geometry:width set-geometry-width!)
  (height geometry:height set-geometry-height!)
  (margin geometry:margin set-geometry-margin!))

(define* (make-geometry #:key (width 1400) (height 900) (margin 80))
  (make-geometry* width height margin))

(define-record-type <theme>
  (make-theme* ui-bg board-bg board-grid disc-black disc-white)
  theme?
  (ui-bg theme:ui-bg set-theme-ui-bg!)
  (board-bg theme:board-bg set-theme-board-bg!)
  (board-grid theme:board-grid set-theme-board-grid!)
  (disc-black theme:disc-black set-theme-disc-black!)
  (disc-white theme:disc-white set-theme-disc-white!))

(define* (make-theme #:key
                     (ui-bg black)
                     (board-bg (make-color 0.1 1 0.1 0.6))
                     (board-grid black)
                     (disc-black black)
                     (disc-white tango-aluminium-1))
  (make-theme* ui-bg board-bg board-grid disc-black disc-white))

(define-record-type <ui-cache>
  (make-ui-cache* origin size spacing line-width radius)
  ui-cache?
  (origin ui-cache:origin set-ui-cache-origin!)
  (size ui-cache:size set-ui-cache-size!)
  (spacing ui-cache:spacing set-ui-cache-spacing!)
  (line-width ui-cache:line-width set-ui-cache-line-width!)
  (radius ui-cache:radius set-ui-cache-radius!))

(define* (make-ui-cache #:key origin size spacing line-width radius)
  (make-ui-cache* origin size spacing line-width radius))

(define-record-type <ui>
  (make-ui* cache geometry theme game-canvas disc-canvas)
  ui?
  (cache ui:cache set-ui-cache!)
  (geometry ui:geometry set-ui-geometry!)
  (theme ui:theme set-ui-theme!)
  (game-canvas ui:game-canvas set-ui-game-canvas!)
  (disc-canvas ui:disc-canvas set-ui-disc-canvas!))

(define (update-ui-cache! ui)
  (let ((cache (ui:cache ui)))
    (set-ui-cache-origin! cache (board-origin ui))
    (set-ui-cache-size! cache (board-size ui))
    (set-ui-cache-spacing! cache (grid-spacing ui))
    (set-ui-cache-line-width! cache (grid-line-width ui))
    (set-ui-cache-radius! cache (disc-radius ui))))

(define (board-origin ui)
  (let ((m (geometry:margin (ui:geometry ui))))
    (vec2 m m)))

(define (board-size ui)
  (let* ((g (ui:geometry ui))
         (m (geometry:margin g))
         (h (geometry:height g)))
    (- h (* 2 m))))

(define (grid-spacing ui)
  (/ (board-size ui) 8.0))

(define (grid-line-width ui)
  2)

(define (disc-radius ui)
  (let ((gs (grid-spacing ui)))
    (/ (- gs (* gs 0.15)) 2.0)))

(define* (make-ui #:key
                  (cache (make-ui-cache))
                  (geometry (make-geometry))
                  (theme (make-theme))
                  game-canvas disc-canvas)
  (let ((ui (make-ui* cache geometry theme game-canvas disc-canvas)))
    (update-ui-cache! ui)
    ui))

(define reversi-ui (make-ui))
(define reversi-game (make-game))

(define (draw alpha)
  (draw-canvas (ui:game-canvas reversi-ui))
  (draw-canvas (ui:disc-canvas reversi-ui)))

(define (update-discs!)
  (define discs
    (position->discs (car (game:positions reversi-game))))
  (set-ui-disc-canvas! reversi-ui (make-canvas (apply superimpose discs))))

(define (init)
  (define bg
    (with-style ((stroke-color tango-aluminium-4)
                 (stroke-width 20)
                 (fill-color black))
                (fill-and-stroke
                 (rectangle (vec2 0 0)
                            (geometry:width (ui:geometry reversi-ui))
                            (geometry:height (ui:geometry reversi-ui))))))
  (define board-bg
    (with-style ((fill-color (make-color 0.1 1 0.1 0.6)))
                (fill (square (board-origin reversi-ui)
                              (board-size reversi-ui)))))
  (define grid
    (apply superimpose
           (append-ec (: n 0 9)
                      (list (horizontal n)
                            (vertical n)))))
  (update-discs!)
  (set-ui-game-canvas! reversi-ui (make-canvas (superimpose bg board-bg grid))))

(define (x->file x)
  (inexact->exact (ceiling (/ x (grid-spacing reversi-ui)))))

(define (y->rank y)
  (inexact->exact (ceiling (/ (- (board-size reversi-ui) y)
                              (grid-spacing reversi-ui)))))

(define (coordinate->location x y)
  (let* ((origin (board-origin reversi-ui))
         (b (car (game:positions reversi-game)))
         (f (x->file (- x (vec2-x origin))))
         (r (y->rank (- y (vec2-y origin)))))
    (and (on-board? b f r)
         (make-location f r))))

(define grid-line
  (with-style ((stroke-color black)
               (stroke-width (grid-line-width reversi-ui)))
    (stroke (line (vec2 0 0) (vec2 (board-size reversi-ui) 0)))))

(define (horizontal n)
  (translate (vec2+ (board-origin reversi-ui)
                    (vec2 0 (* n (grid-spacing reversi-ui))))
    grid-line))

(define (vertical n)
  (translate (vec2+ (board-origin reversi-ui)
                    (vec2 (* n (grid-spacing reversi-ui)) 0))
    (rotate (degrees->radians 270) grid-line)))

(define (position n)
  (let ((s (grid-spacing reversi-ui)))
    (+ (/ s 2)
       (* s (1- n)))))

(define (x-position f)
  (position f))

(define (y-position r)
  (- (board-size reversi-ui) (position r)))

(define (grid-coordinate f r)
  (vec2+ (board-origin reversi-ui) (vec2 (x-position f) (y-position r))))

(define (make-disc f r colour)
  (with-style ((fill-color colour))
    (fill (circle (grid-coordinate f r) (disc-radius reversi-ui)))))

(define (make-disc-from-spec spec)
  (match spec
    ((f r 'black) (make-disc f r black))
    ((f r 'white) (make-disc f r tango-aluminium-1))
    (_ (error (format #f "Invalid disc spec: ~a" spec)))))

(define (make-discs l)
  (map make-disc-from-spec l))

(define (position->discs p)
  ((compose make-discs discard-empty annotate-position) p))
