PROJECT = guile-reversi
TOPDIR = .

LOAD_PATH = $(TOPDIR)/scheme
TEST_PATH = $(TOPDIR)/tests

GUILE_BINARY ?= guile
GUILE_CALL = $(GUILE_BINARY) -L $(LOAD_PATH) -C $(LOAD_PATH) --no-auto-compile
GUILD_BINARY ?= guild

RUNTESTS = SCHEME_INTERPRETER="$(GUILE_BINARY)" run-tests
RUNTESTS += -strip-roots -dispatch-root "$(TEST_PATH)"
INSTALL = $(GUILE_CALL) $(TOPDIR)/tools/install

CFLAGS = -Wunsupported-warning
# These results in lots of warnings from record-type definitions, so I'm
# turning them off for now:
#CFLAGS += -Wunused-variable -Wunused-toplevel
CFLAGS += -Wunbound-variable -Warity-mismatch -Wduplicate-case-datum
CFLAGS += -Wbad-case-datum -Wformat -L$(LOAD_PATH)

COMPILE = $(GUILD_BINARY) compile $(CFLAGS)

TESTGUILE = ./tools/guile-in-here
PROVE = tap-harness -e '$(TESTGUILE)'

MODULES  = ./scheme/reversi/game.scm
MODULES += ./scheme/reversi/program.scm
MODULES += ./scheme/reversi/ui.scm
MODULES += ./scheme/reversi/input/keyboard.scm
MODULES += ./scheme/reversi/input/mouse.scm

OBJECTS = ${MODULES:.scm=.go}
TESTS = $(TEST_PATH)/*-scm.t

.SUFFIXES: .scm .go

all: $(OBJECTS)

.scm.go:
	$(COMPILE) -o $@ $<

test:
	$(PROVE) $(TESTS)

test-verbose:
	$(PROVE) --verbose $(TESTS)

install: all
	$(INSTALL) DESTDIR="$(DESTDIR)" DOCDIR="$(DOCDIR)" PREFIX="$(PREFIX)"

clean:
	find . -name "*.go" -exec rm -f '{}' +
	find . -name "*~" -exec rm -f '{}' +

.PHONY: all clean install test
